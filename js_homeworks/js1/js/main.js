const getName = () => {
    let userName = prompt("What's your name?");
    while (isFinite(parseInt(userName))){
        console.log(userName);
        userName = prompt(`Is it your name: ${userName}.\nWhat's your real name?`);
    }
    return userName;
}
const getAge = () => {
    let userAge = prompt("How old are you?");
    while (isNaN(parseInt(userAge))){
        console.log(userAge);
        userAge = prompt(`Is it your age: ${userAge}.\nHow old are you?`);
    }
    return userAge;
}   

const verifyAge = () => {
     const name = getName();
     const age = getAge();
     
     const noobMsg = "You are not allowed to visit this website";
     const proMsg = `Welcome, ${name}`;
     
     if(age < 18){
         return alert(noobMsg);
     } else if(age < 23){
         confirmation = confirm("Are you sure you want to continue?");
         return (confirmation == true) ? alert(proMsg):alert(noobMsg);
     } else if(23 < age){
         return alert(proMsg);
     }
}

verifyAge();