// let iconPass = document.querySelectorAll('.icon-password');
let passForm = document.querySelector('.password-form');
let inputPassIcons = document.querySelectorAll('.icon-password');

const togglePassVisibility = (item) => {
    let input = document.querySelector(`input[data-input-id="${item.dataset.iconId}"]`);
    if(item.classList.contains('fa-eye')){
        input.setAttribute('type', 'text');
    } else if(item.classList.contains('fa-eye-slash')){
        input.setAttribute('type', 'password');
    }
}

const togglePassIcon = (item) => {
    item.classList.toggle('fa-eye');
    item.classList.toggle('fa-eye-slash');
}

const validatePass = () => {
    hideError();
    let pass = document.querySelector('input[data-input-id="get-password"]');
    let passConfirm = document.querySelector('input[data-input-id="confirm-password"]');
    if(pass.value === passConfirm.value){
        alert('You\'re welcome!');
    } else{
        showError('Нужно ввести одинаковые значения', passConfirm.parentNode);
    }
}

const showError = (msg, node) => {
    let passErrorMsg = document.createElement('span');
    passErrorMsg.classList.add('error');
    passErrorMsg.innerText = msg;
    node.after(passErrorMsg);
}

const hideError = () => {
    let passErrorMsg = document.querySelector('.error');
    if(passErrorMsg) passErrorMsg.remove();
}

document.addEventListener("DOMContentLoaded", () => {
    Array.from(inputPassIcons).forEach(function(node){
        if(node.classList.contains('fa-eye')) togglePassIcon(node);
        togglePassVisibility(node);
    })
})

passForm.addEventListener('click', {
    handleEvent(event){
        let child = event.target;
        let type = child.dataset.nodeType
        if(type === 'icon-password'){
            togglePassIcon(child);
            togglePassVisibility(child);
        }else if(type === 'submit'){
            validatePass();
        }
    }
})
