let btnKeys = document.querySelector('.btn-wrapper').children;
let btnKeysArray = Array.from(btnKeys);


const getNodesValues = (arr) => {
    return arr.map(function(element){
        return element.dataset.keyCode;
    });
}

document.addEventListener('keydown', function(event){
    const keys = getNodesValues(btnKeysArray);
    
    if(keys.includes(event.code)){
        let btnActiveKey = document.querySelector('.active');
        if(btnActiveKey) btnActiveKey.classList.remove('active');
        
        let btnKey = document.querySelector(`button[data-key-code=${event.code}]`);
        btnKey.classList.add('active');
    }
})