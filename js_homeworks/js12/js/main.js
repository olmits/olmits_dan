let images = document.getElementsByClassName('image-to-show');
let imagesContainer = document.querySelector('.images-wrapper');
let imagesIterable = Array.from(images);

function setToggleUI() {
    let stopBtn = document.createElement('button');
    stopBtn.setAttribute('data-btn', 'stop');
    stopBtn.innerText = 'STOP';

    let resumeBtn = document.createElement('button');
    resumeBtn.setAttribute('data-btn', 'renew');
    resumeBtn.innerText = 'RENEW';

    imagesContainer.after(resumeBtn);
    imagesContainer.after(stopBtn);

    return {resume: resumeBtn, stop:stopBtn}
}

function setImgVisible(img) {
    imagesIterable.map(function(imgNode){
        imgNode.style.display = 'none'
    })
    img.setAttribute('style', '');
}

let imageIndex = 0;
setImgVisible(images[imageIndex]);

function selectImg() {
    imageIndex++
    if(imageIndex >= images.length) {
        imageIndex = 0;
    }
    let currentImage = images[imageIndex]
    setImgVisible(currentImage);
}
function iniateTimer(fun, delay){
    return setInterval(() => {
        fun();
    }, delay);
}

let imageTimer = iniateTimer(selectImg, 1000);

let toggleUI = setToggleUI();
toggleUI.stop.addEventListener('click', () => {
    clearInterval(imageTimer);
    console.log(`Stopped on the ${imageIndex} image`);
});
toggleUI.resume.addEventListener('click', () => {
    imageTimer = iniateTimer(selectImg, 1000);
    console.log(`Iniated from the ${imageIndex} image`);
})