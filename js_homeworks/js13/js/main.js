function defineCSSProps(name, backgroundColor, fontColor1, fontColor2){
    let props = new Object();
    Object.defineProperties(props, {
        'name': {
            get() {
                return this._name;
            },
            set(value) {
                this._name = value;
            }
        },
        'background-color': {
            get() {
                return this._background;
            },
            set(value) {
                console.log(`Background color set to ${value}`);
                this._background = value;
            }
        },
        'font-color1': {
            get() {
                return this._fontColor;
            },
            set(value) {
                console.log(`Font color1 set to ${value}`);
                this._fontColor = value;
            }
        },
        'font-color2': {
            get() {
                return this._fontColor;
            },
            set(value) {
                console.log(`Font color2 set to ${value}`);
                this._fontColor = value;
            }
        },
        'getStorageProps': {
            value: function(){
                let storageContent = localStorage.getItem('cssProps');
                if (storageContent) {
                    let cssProps = JSON.parse(storageContent);
                    this.name = cssProps.name
                    this.backgroundColor = cssProps.backgroundColor;
                    this.fontColor1 = cssProps.fontColor1;
                    this.fontColor2 = cssProps.fontColor2;
                }
                return this
            },
            writable: false
        },
        'setStorageProps': {
            value: function(){
                if(this){
                    let propsSerialized = JSON.stringify(this);
                    localStorage.setItem('cssProps', propsSerialized);
                }
            }
        }
    })

    props.name = name;
    props.backgroundColor = backgroundColor;
    props.fontColor1 = fontColor1;
    props.fontColor2 = fontColor2;
    
    return props
}

let theme0 = defineCSSProps('theme0', '#FFFFFF', '#434343', '#3cb878');
let theme1 = defineCSSProps('theme1', '#000000', '#FFFFFF', '#e12813');
let themes = {'theme0': theme0, 'theme1': theme1};

let processableNodes = document.getElementsByClassName('theme-processable');
let nodeaArray = Array.from(processableNodes);
let themeSwitcher = document.querySelector('.content__header-theme__switcher > button');

function setTheme(themeObject){
     
    nodeaArray.forEach(function(item){

        let currentTarget = item.dataset.themeTarget;
        item.dataset.theme = themeObject.name
        switch(currentTarget) {
            case 'background':
                item.style.background = themeObject.backgroundColor;
                break
            case 'color1':
                item.style.color = themeObject.fontColor1;
                break
            case 'color2':
                item.style.color = themeObject.fontColor2;
                break
        }
    })
}

document.addEventListener('DOMContentLoaded', function(){
    let themeObject = defineCSSProps();
    themeObject.getStorageProps();
    
    setTheme(themeObject)
})

themeSwitcher.addEventListener('click', function(){
    let currentTheme = nodeaArray[0].dataset.theme;
    let newThemeName = (currentTheme === 'theme0') ? 'theme1' : 'theme0';
    let newThemeObject = themes[newThemeName];
    
    setTheme(newThemeObject);
    newThemeObject.setStorageProps();
})