$(".tabs-title").click(function(){
    $(".tabs-title").removeClass("active");
    $(this).addClass("active");
    console.log($(this).data('tbKey'));
    
    let activeKey = $(this).data('tbKey');
    $(".tabs-content > li").hide();
    $(`.tabs-content > li[data-tb-value="${activeKey}"]`).show()
});

$(document).ready(function(){
    let activeKey = $(".tabs-title.active").data('tbKey');
    $(".tabs-content > li").hide();
    $(`.tabs-content > li[data-tb-value="${activeKey}"]`).show()
})