let winHeight = $(window).height();

$(document).ready(function(){
    $('.back-top__btn').hide();
})

$(window).scroll(function(){
    if(winHeight < $(window).scrollTop()){
        $('.back-top__btn').show();
    } else {
        $('.back-top__btn').hide();
    }
})

function scrollToAnchor(anchor){
    let anchorTagOffset = $(`a[name="${anchor}"]`).offset().top;
    let anchorBtnOffset = $(`a[href="#${anchor}"]`).offset().top;
    console.log(anchorBtnOffset);
    console.log(anchorTagOffset);
    $('html,body').animate({scrollTop: anchorTagOffset}, 'slow');
}

$('.arch__navigation-item > a').click(function(){
    let anchor = this.hash.substr(1);
    scrollToAnchor(anchor);
})
$('.back-top__btn').click(function(){
    let anchor = this.hash.substr(1);
    scrollToAnchor(anchor);
})


