const validateNumber = (num) => {
    if(isNaN(num)){
        return false;
    } return true;
} 

const getNum = () => {
    let userInput = prompt("Type a number: ");
    while(!validateNumber(userInput)){
        userInput = prompt("Huh? Type another one: ", userInput);
    }
    return userInput;
}

function getFactorial1(num){
    if(num > 0){
        return num * getFactorial(num-1);
    } return 1;
}

const getFactorial2 = (num) => {
    return (num > 1) ? num * getFactorial2(num - 1) : num;
}

num = getNum()
r = getFactorial2(num);
console.log(r);