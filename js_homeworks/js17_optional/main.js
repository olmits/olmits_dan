const validateNumber = (num) => {
    if(isNaN(num)){
        return false;
    } return true;
} 

const getNum = () => {
    let userInput = prompt("Type a number: ");
    while(!validateNumber(userInput)){
        userInput = prompt("Huh? Type another one: ", userInput);
    }
    return userInput;
}

const getFibi = (f0, f1, num) => {
    if(num < 0) {
        return (num * (-1) !== 2) ? getFibi(f1, f0 + f1, num + 1) : f1;
    } else {
        return (num !== 2) ? getFibi(f1, f0 + f1, num - 1) : f1;
    } 
    
}

// -34, -21, -13, -8, [-5], -3, -2, -1, -1, 0
rNegative = getFibi(0, 1, -6); // -5
// 0, 1, 1, 2, 3, [5], 8, 13, 21, 34, 55, 89
rPositive = getFibi(0, 1, 6); // 5

numUser = getNum();
rUser = getFibi(0, 1, numUser);
console.log(rUser);
alert(`Leonardo Pisano say ${rUser}`);