function getNumbers(){

    const validateNumbers = ([...inputNumbers]) => {
        nanExist = inputNumbers.some(function(item){
            return isNaN(item) || !(item % 1 === 0) || (item === "") || (item === null); 
        });
        if(nanExist){
            return false
        } return true
    }
    
    let num1=prompt("Type a number: "), num2=prompt("Type another one: ");
    let userNumbers = new Array(num1, num2);

    while(!validateNumbers(userNumbers)){
        console.log({error:`What - ${userNumbers}?`});
        num1=prompt("There was a mistyped in your input.\nType a number: ");
        num2=prompt("Still fixing a trouble.\nType another one: ");
        userNumbers = [num1, num2];
    };

    return userNumbers.sort();
}

function returnFifthDiv([a,b]){
    if(b-a >= 5){
        document.write(`<p>Numbers divisible by 5 from ${a} to ${b}:</p>`);
        document.write('<ul>');
        for (i=a; i<b; i++){
            if(i%5 === 0){
                console.log(i);
                document.write(`<li>${i}</li>`);
            }
        }
        document.write('</ul>');
    }else{
        console.log(`Sorry, no numbers divisible by 5. Range: ${a} and ${b}`);
        document.write(`<p>Sorry, no numbers divisible by 5 between ${a} and ${b}</p>`);
    }
}

function returnNumbers([a,b]){
    if(a-b !== 0){
        document.write(`<p>Prime numbers from ${a} to ${b}:</p>`);
        document.write('<ul>');
        for (i=a; i<b; i++){
            console.log(i);
            document.write(`<li>${i}</li>`);
        }
        document.write('</ul>');
    }else{
        console.log(`Sorry, no numbers. Range: ${a} and ${b}`);
        document.write(`<p>Sorry, no numbers between ${a} and ${b}</p>`);
    }
}

numRange = getNumbers();
returnFifthDiv(numRange);
document.write('<hr>');
returnNumbers(numRange);