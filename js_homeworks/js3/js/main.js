const validateNumbers = ([...inputNumbers]) => {
    nanExist = inputNumbers.some(function(item){
        return isNaN(item) || !(item % 1 === 0) || (item === "") || (item === null); 
    });
    if(nanExist){
        return false
    } return true
}

const getNumbers = function(){
    
    let num1=prompt("Type a number: "), num2=prompt("Type another one: ");
    let userNumbers = new Array(num1, num2);

    while(!validateNumbers(userNumbers)){
        console.log({error:`What - ${userNumbers}?`});
        num1=prompt("There was a mistyped in your input.\nType a number: ", userNumbers[0]);
        num2=prompt("Still fixing a trouble.\nType another one: ", userNumbers[1]);
        userNumbers = [num1, num2];
    };
    return userNumbers;
}

const getOperation = function(){

    const mathOperations = new Array('+','-','*','/');
    let mathOp = prompt("What will it be?\nSum up: +\nSubtract: -\nMultiply: *\nDivide: /");

    while(!mathOperations.includes(mathOp)){
        console.log({error: `Invalide input: ${mathOp}`});
        mathOp = prompt("There was a mistyped in your input.\nAnother try?");
    }
    return mathOp;
}

const proceedCalc = function(num1, num2, operation){
    switch(operation){
        case '+':
            return (parseInt(num1) + parseInt(num2));
        case '-':
            return (parseInt(num1) - parseInt(num2));
        case '*':
            return (parseInt(num1) * parseInt(num2));
        case '/':
            return (parseInt(num1) / parseInt(num2));
        default:
            return 'Something wrong. Address debugger.'
    }
}

numArr = getNumbers();
opp = getOperation();
result = proceedCalc(numArr[0], numArr[1], opp);

document.write(`<p>${numArr[0]} ${opp} ${numArr[1]} = ${result}</p>`)