function createNewUser(fName, lName) {
    let newUser = new Object();
    Object.defineProperty(newUser, 'firstName', {
        get() {
            return this._firstName;
        },
        set(value) {
            if(value === null){
                alert('You are null. Try again.')
            }else if(value === ""){
                alert('You are undefined. Try again.')
            }
            this._firstName = value;
        }
    });
    Object.defineProperty(newUser, 'lastName', {
        get() {
            return this._lastName;
        },
        set(value) {
            if(value === null){
                alert('You are null. Try again.')
            }else if(value === ""){
                alert('You are undefined. Try again.')
            }
            this._lastName = value;
        }
    });
    Object.defineProperty(newUser, 'getLogin', {
        value: function(){
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        writable: false
    })

    let userFirstName = fName || prompt("What is your first name?");
    newUser.firstName = userFirstName;
    
    let userLastName = lName || prompt("What is your last name?");
    newUser.lastName = userLastName;

    return newUser
}


testUser = createNewUser("Scarlett", "Johansson");
testLogin = testUser.getLogin();
console.log(testUser);
console.log(testLogin);

myUser = createNewUser(); // Martin Schoeller
myLogin = myUser.getLogin();
console.log(myUser); // {_firstName: "Martin", _lastName: "Schoeller", getLogin: ƒ}
console.log(myLogin); // mschoeller
