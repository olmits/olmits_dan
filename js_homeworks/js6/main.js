const filterBy = function(data, dataType) {
    if(data.some(function(item){
        return typeof(item) == dataType;
    })){
        let dataCleared = data.filter(function(element) {
            return typeof(element) !== dataType;
        });
        return dataCleared;
    } return data;
}


dataType = 'number';
testData = [1, 2, "John Snow", "You know nothing about JS", null, true];

r = filterBy(testData, dataType);
console.log(r);