function createList(data, level=1) {
    let it = data.map(function(elem, index){
        if(typeof(elem) === "object"){
            return createList(elem, level + 1);
        } else{
            return `<li class="data__items-${level} item-${index+1}">${elem}</li>`
        }
    })
    return `<ol class="data__list data-${level}">${it.join('')}</ol>`
}

function revealArray(data){
    const container = document.getElementById('container');
    let dataPrep = createList(data);

    container.innerHTML = dataPrep;
}

function clearAll(){
    const main = document.body;
    while(main.firstElementChild){
        main.removeChild(main.firstElementChild);
    }
    main.style.background = 'rgba(145, 87, 203, 0.1)';
}

team2 = ['Weedle', 'Spearow', 'Sandslash', 'Paras']
team1 = ['Pyatov', 'Kryvtsov', 'Stepanenko', 'Yarmolenko', 'Malinovskyi', team2, 'Yaremchuk', 'Marlos', 'Mykolenko', 'Zinchenko', 'Karavaev', 'Matviyenko']
team0 = ['Sodium', 'Potassium', 'Rubidium', 'Caesium', team1, 'Francium'];
revealArray(team0);


let tick = 10;

function revealTimer(){
    let timer = document.querySelector('.timer');

    if(timer instanceof HTMLElement){
        timer.style.opacity = 1;
        timer.innerText = tick;
    } else{
        let timeWrapper = document.createElement('div');
        timeWrapper.classList.add('time_wrapper');
        timeWrapper.innerHTML = `<div class="timer">${tick}</div>`
        document.body.append(timeWrapper);
    }
    tick--;
}

let countDown = setInterval(revealTimer, 1000);
setTimeout(() => {
    clearInterval(countDown);
    clearAll();
}, tick*1000 + 1000);



