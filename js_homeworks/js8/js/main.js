const setAttributes = (elem, attrs) => {
    for(let key in attrs){
        if(attrs[key]) { elem.setAttribute(key, attrs[key]) };
    }
}

function createInputField(form, data = 'price', name = 'price', type = 'number', placeholder = '$0.00'){
    let input = document.createElement('input');
    
    attrs = {'type': type, 'name': name, 'placeholder': placeholder, 'data-type': data};
    setAttributes(input, attrs);

    let formChildren = Array.from(form.childNodes);
    let formChild = document.querySelector(`.${form.className} > input[data-type='${data}']`);
    if(formChildren.includes(formChild)) return;
    
    input.classList.add(form.className+'-item', 'item-'+data);
    form.append(input);
    return input;
}

function revealError(elem, issue) {
    let errorSpan = document.createElement('span');
    errorSpan.classList.add('form__container-error__msg');
    errorSpan.innerText = `Please enter correct ${issue}`;
    elem.after(errorSpan);
}
function hideError() {
    let errorSpan = document.querySelector('.form__container-error__msg');
    if(!errorSpan) return;
    errorSpan.parentElement.removeChild(errorSpan);
}

// SECTION: Form filling
let formSection = document.querySelector('.form__container');
let spanContainer = document.querySelector('.form__container-span__filter');
let newInput1 = createInputField(formSection, 'price', 'price', 'text', '$0.00');
let newInput2 = createInputField(formSection, 'product', 'product', 'text', 'Product');


// SECTION: Form event listener
formSection.addEventListener('keyup', {
    handleEvent(event){
        const validateCurrency = (numString) =>  numString.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ","); // Function to pretify currency number

        let input = event.target;
        let value = input.value;
        if(value === '') return;
        switch(input.dataset.type){
            case 'price':
                value[0] === '-' ? valueSign = '-' : valueSign = '';
                if(value.indexOf('.') >= 0){
                    let decimalPos = value.indexOf('.');
                    let leftSide = value.substring(0, decimalPos);
                    let rightSide = value.substring(decimalPos);

                    leftSide = validateCurrency(leftSide);
                    rightSide = validateCurrency(rightSide);

                    rightSide = rightSide.substring(0, 2);
                    newValue = `${valueSign}${leftSide}.${rightSide}`;
                    input.setAttribute('data-value', `${valueSign}${leftSide.replace(/\D/g, "")}.${rightSide}`);
                } else{
                    newValue = validateCurrency(value);
                    newValue = `${valueSign}${newValue}`;
                    input.setAttribute('data-value', valueSign + newValue.replace(/\D/g, ""));
                }
                input.value = newValue;
                break
            default:
                input.setAttribute('data-value', `${value}`);
        }
    }
})

for(let node of formSection.childNodes){
    
    // SECTION: Event listener for "focus"
    node.addEventListener('focus', function(){
        this.style.border = '1px solid green';
        this.style.width = '200px';
    })

    // SECTION: Event listener for "unfocus"
    node.addEventListener('blur', function(){
        let span = document.querySelector(`.form__container-span__filter > span[data-type='${this.dataset.type}']`);
        
        if(parseFloat(this.value) < 0) {
            this.style.border = '1px solid red';
            if(span){spanContainer.removeChild(span)};
            if(document.querySelector('.form__container-error__msg')) return;
            revealError(formSection, this.dataset.type);
            return
        } else {
            hideError()
        };

        if(this.value){
            if(span){
                span.innerHTML = `Current ${this.dataset.type}: ${this.dataset.value}&#160<i class="far fa-times-circle"></i>`;
            }else{
                let span = document.createElement('span');
                span.setAttribute('data-type', this.dataset.type);
                span.innerHTML = `Current ${this.dataset.type}: ${this.dataset.value}&#160<i class="far fa-times-circle"></i>`;
                spanContainer.append(span);
            }
            spanContainer.style.visibility = 'visible';
        } else if(!this.value){
            this.removeAttribute('style');
            let span = spanContainer.querySelector(`.form__container-span__filter > span[data-type='${this.dataset.type}']`);
            if(span){spanContainer.removeChild(span)}
            if (spanContainer.childNodes.length === 0) {
                spanContainer.style.visibility = 'hidden';
            }
        }
    })
}


// SECTION: Span event listener
spanContainer.addEventListener('click', {
    handleEvent(event){
        if(event.target.classList.contains('fa-times-circle')){
            let span = event.target.parentNode;
            let input = document.querySelector(`input[data-type='${span.dataset.type}']`);
            input.value = '';
            input.removeAttribute('style');
            spanContainer.removeChild(span);
        }
    }
});