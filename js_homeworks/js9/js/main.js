let tabs = document.querySelector('.centered-content > .tabs');
let tabsContent = document.querySelector('.centered-content > .tabs-content');

const clearActive = (elem) => { // Deactivate all tabs
    let elemChilds = Array.from(elem.children);
    elemChilds.forEach(function(child){
        child.dataset.active = 'false';
        child.classList.remove('active');
    })
}

const hideContent = (elem) => { // Hide all content blocks
    let elemChilds = Array.from(elem.children);
    elemChilds.forEach(function(child){
        child.dataset.active = 'false';
        child.style.display = 'none';
    })
}

const revealContent = (tbkey) => {
    let keyValue = document.querySelector(`li[data-tb-value="${tbkey}"]`)
    keyValue.style.display = 'block';
}

tabs.addEventListener('click', {
    handleEvent(event){
        clearActive(tabs);
        hideContent(tabsContent);
        
        let item = event.target;
        item.classList.add('active');

        let key = item.dataset.tbKey;
        revealContent(key);
    }
})

document.addEventListener('DOMContentLoaded', () => {
    hideContent(tabsContent);
    
    let activeTab = document.querySelector('li[data-active="true"]');
    let activeKey = activeTab.dataset.tbKey;
    revealContent(activeKey);
});

